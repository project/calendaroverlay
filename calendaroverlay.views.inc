<?php
/**
 * hook post execute views hook - take only my clients
 */
function calendaroverlay_views_post_execute(&$view){
  $views_data = variable_get('calendaroverlay_views',array());
  
  foreach ($views_data as $data) {
    if ($view->name == $data['name']) {
      calendaroverlay_init_js();
      calendaroverlay_node_type($data['type'],$data['time']);
    } 
  }
  
}