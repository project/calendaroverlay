/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function($) {
   Drupal.behaviors.calendaroverlay = {
     
     attach: function(context){
       
      //time slots 
      var date_table = $('#single-day-container');
      
      if (date_table.length == 0) {
        return;
      }

      var slots = $('#single-day-container .single-day');
      
      slots.each(function(index){
        time_grid_x = $(this).width();//a day offset
        time_grid_y = $(this).height()+1;//a hour offset
        var timestamp = $(this).attr('id');
      });
      var main_object = this;
      var time_grid_x;
      var time_grid_y;
      this.node_add();//get add node options
      //permission to resize and dragg
      if (Drupal.settings.calenderoverlay_timeupdate.edit_access == 'access') {
        //node objects
        var objects = $('#single-day-container .single-day .inner').children();
        //set as draggable
        objects.each(function(index){
          var container = $(this).children().children().children();
          var c_widht = container.width();
          //resisze
          container.resizable( "destroy" );
          container.resizable({handles: "s", grid: time_grid_y/4,
            stop: function( event, ui ) {
              //fire ajax to update nodes 
              var dragged_item = $(this);
              var data = main_object.node_data(dragged_item);
              var set_time = main_object.time_calculation(data, ui, time_grid_x, time_grid_y);
              main_object.node_update(data, set_time);
            }
          });
          container.draggable( "destroy" );
          container.draggable({ grid: [ time_grid_x, time_grid_y/4 ],snap: "td",containment: "#single-day-container",
            stop: function(event, ui){
              //fire ajax to update nodes
              var dragged_item = $(this);
              var is_overlap = main_object.node_validate(dragged_item);

              if (is_overlap){
                alert(Drupal.t('Time of nodes are ovelaping. Move the node to empty space'));
              }else{
                var data = main_object.node_data(dragged_item);
                var set_time = main_object.time_calculation(data, ui, time_grid_x, time_grid_y);
                main_object.node_update(data, set_time);
              }

            }
          });
        
        });
      }
      
      //init buttons
      this.node_edit();
      this.node_delete();
      
     },
     /**
      * check if times of node overlaps
      * @param node - dom element
      */
     node_validate: function(node) {
       //get all nodes
       var nodes = $('#single-day-container .single-day .inner').children();
       var is_overlap = false;
       function compare (el1, el2) {
          var rect1 = {},
              rect2 = {};
          rect1.top = parseFloat(el1.offset().top);
          rect1.left = parseFloat(el1.offset().left);
          rect1.right = parseFloat(el1.width());
          rect1.bottom = parseFloat(el1.height())+rect1.top;
          rect2.top = parseFloat(el2.offset().top);
          rect2.left = parseFloat(el2.offset().left);
          rect2.right = parseFloat(el2.width());
          rect2.bottom = parseFloat(el2.height())+rect2.top;
          var is_top = rect2.top>rect1.top && rect2.top<rect1.bottom;
          var is_bottom = rect2.bottom>rect1.top && rect2.bottom<rect1.bottom;
        
          var is_colide = (is_top || is_bottom) &&
              Math.abs(rect1.left-rect2.left)<20;
          return is_colide;
       }
       //compare with other nodes
       nodes.each(function(index){
         var container = $(this).children().children().children();
         var result = false;
         var val1 = container.get(0);
         var val2 = node.get(0);
         if (val1 !== val2){//exlude compare with itself
          result = compare(container,node);
          if (result){
            is_overlap = result;
          }
         
         }
         
       });
       
       return is_overlap;
     },
     /**
      * calculate time from position of node on calendar area
      * @param data - input data
      * @param time_grid_x - x position of node
      * @param time_grid_y - y position of node
      * @param ui - moving element
      */
     time_calculation: function(data,ui,time_grid_x,time_grid_y) {
      
       var x_offset = parseFloat(ui.position.left);
       var y_offset = parseFloat(ui.position.top);
       //get node start and end time
       var start_time_node = parseFloat(data['start']);
       var end_time_node = parseFloat(data['end']);
       
       var duration = parseFloat(data['end']) - parseFloat(data['start']);
       var height = parseFloat(ui.helper.height());
       var new_duration_hours = Math.round(height/parseFloat(time_grid_y/4))*0.25;
      
       var day_time_offset = Math.round(x_offset/time_grid_x);
       var hour_time_offset = parseFloat(y_offset)/parseFloat(time_grid_y);
      
       var new_offset_day = day_time_offset*24*60*60;
       var new_offset_hour = Math.round(hour_time_offset*100)*60*60/100;
       var new_offset_hours_end = new_duration_hours*60*60;
       
       var return_date = [];
       //we are usign absolute time
       return_date['start_offset'] = start_time_node+parseFloat(new_offset_day)+parseFloat(new_offset_hour);
       return_date['end_offset'] = return_date['start_offset'] + new_offset_hours_end; 
       return return_date;
     },
     /**
      * update node to database
      * @param data - input data
      * @param offset - offset time from original position
      * @param field_name - date field name
      * 
      */
     node_update: function(data,offset_time,field_name) {
        var post_data = {};
        var main_object = this;
        var update_url = Drupal.settings.calenderoverlay_timeupdate.json_url;
        var field_name = Drupal.settings.calenderoverlay_timeupdate.field_name;
        post_data.node_id = data['id'];
        post_data.start_offset = offset_time['start_offset'];
        post_data.end_offset = offset_time['end_offset'];
        post_data.field_name = field_name;
        var item = data['item'];
        $.ajax({
            url: update_url,
            data: post_data,
            type: 'POST', 
            success: function(result) {
              var result_start = result.start;
              var start_split = result_start.split(' ');
              var result_end = result.end;
              var end_split = result_end.split(' ');
              
              var time_node = item.find('.date-display-single');
              var text_time = time_node.html();
              var extract_time = text_time.substring(10);
              
              extract_time = start_split[0]+extract_time;
              time_node.html(extract_time);
              var start_time = time_node.children('.date-display-start').text(start_split[2]);
              var end_time = time_node.children('.date-display-end').text(end_split[2]);
              $(time_node).trigger('value_changed');
            }
        });
     },
     /**
      * get data from calendar slot
      * @param - slot
      */
     node_data: function(item) {
       var node_data = item.parent().parent().parent().attr("id");
       var data = node_data.split("-");
       var node = [];
       node['id'] = data[1];
       node['start'] = data[2];
       node['end'] = data[3];
       node['item'] = item;
       return node;
     },
     /**
      * edit button function
      */
     node_edit: function () {
      
       $('.cbutton1').each(function(index){
         
         $(this).unbind('click');
         $(this).click(function(env){
           env.preventDefault();
           var href = $(this).attr('href');
           var post_data = {};
           var ajax_url = href;
           var button = $(this);
           post_data.href = href;
           post_data.destination = window.location.pathname;
           ajax_url = ajax_url+'?href='+window.location.pathname;
           var loader = '<div class="calendar-overlay-loader">'+Drupal.t('Loading')+'</div>';
           $('.view').after(loader);
           $.get(ajax_url, function(data) {
             var output = '<div class="c-node-edit"><div class="c-close">X</div>'+data+'</div>';
              $('.c-node-edit').remove();
              $('.calendar-overlay-loader').remove();
              $('.view').after(output);
              var popup = $('.c-node-edit');
              var button_x = button.offset().left;
              var button_y = button.offset().top;
              popup.offset({ top: button_y, left: button_x });
             
              Drupal.behaviors.autocomplete.attach(document, Drupal.settings);
              Drupal.behaviors.verticalTabs.attach(document);
              Drupal.behaviors.filterGuidelines.attach(document);
              $(".c-close").click(function(){
                 $(this).parent().remove();
               });
           });
          
         });
       });
     },
     /**
      * delete button function
      */
     node_delete: function () {
       
       $('.cbutton2').each(function(index){
         $(this).unbind('click');
         $(this).click(function(env){
           env.preventDefault();
           var href = $(this).attr('href');
           var post_data = {};
           var ajax_url = href;
           var button = $(this);
           post_data.href = href;
           post_data.destination = window.location.pathname;
           ajax_url = ajax_url+'?href='+window.location.pathname;
           var loader = '<div class="calendar-overlay-loader">'+Drupal.t('Loading')+'</div>';
           $('.view').after(loader);
           $.get(ajax_url, function(data) {
             var output = '<div class="c-node-delete"><div class="c-close">X</div>'+data+'</div>';
              $('.c-node-delete').remove();
              $('.calendar-overlay-loader').remove();
              $('.view').after(output);
              var popup = $('.c-node-delete');
              var button_x = button.offset().left;
              var button_y = button.offset().top;
              popup.offset({ top: button_y, left: button_x });
              
              $(".c-close").click(function(){
                 $(this).parent().remove();
              });
           });
         });
       });
     },
     /**
      * node add function 
      */
     node_add: function (){
       var slots = $('#single-day-container .single-day');
       slots.each(function(index){
         $(this).unbind('dbclick');
         $(this).dblclick(function(env){
           var id = $(this).attr('id');
           var data = id.split('-');
           var timestamp = data[1];
           var post_data = {};
           var add_url = Drupal.settings.calenderoverlay_node_add.json_url;
           var type = Drupal.settings.calenderoverlay_node_add.type;
           var field_name = Drupal.settings.calenderoverlay_node_add.field_name;
           var button = $(this);
           
           post_data.type = type;
           post_data.timestamp = timestamp;
           post_data.field_name = field_name;
           add_url = add_url + '?type='+type+'&timestamp='+timestamp+'&field_name='+field_name+
           '&href='+window.location.pathname; 
           var loader = '<div class="calendar-overlay-loader">'+Drupal.t('Loading')+'</div>';
           $('.view').after(loader);
           $.get(add_url, function(data) {
             var output = '<div class="c-node-add"><div class="c-close">X</div>'+data+'</div>';
              
              $('.c-node-add').remove();
              $('.calendar-overlay-loader').remove();
              $('.view').after(output);
              var popup = $('.c-node-add');
              var button_x = button.offset().left;
              var button_y = button.offset().top;
              popup.offset({ top: button_y, left: button_x });
              
               Drupal.behaviors.autocomplete.attach(document, Drupal.settings);
               Drupal.behaviors.verticalTabs.attach(document);
               Drupal.behaviors.filterGuidelines.attach(document);
               $(".c-close").click(function(){
                 $(this).parent().remove();
               });
           });
          
         });
       });
     }
   }
})(jQuery);